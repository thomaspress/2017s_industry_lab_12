package ictgradschool.industry.lab12.bounce;

import java.awt.*;

public class DynamicRectangleShape extends Shape {

    private Color color = Color.BLACK;
    private Color temp;
    private boolean topbottom = true;

    public DynamicRectangleShape() {
        super();
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     * @param x x position.
     * @param y y position.
     * @param deltaX speed and direction for horizontal axis.
     * @param deltaY speed and direction for vertical axis.
     */
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY) {
        super(x,y,deltaX,deltaY);
    }

    /**
     * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
     * variables.
     * @param x x position.
     * @param y y position.
     * @param deltaX speed (pixels per move call) and direction for horizontal
     *        axis.
     * @param deltaY speed (pixels per move call) and direction for vertical
     *        axis.
     * @param width width in pixels.
     * @param height height in pixels.
     */
    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x,y,deltaX,deltaY,width,height);
    }

    public DynamicRectangleShape(int x, int y, int deltaX, int deltaY, Color color) {
        super(x,y,deltaX,deltaY);
        this.temp = color;
    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fWidth,fHeight);
        painter.setColor(this.color);
        if (topbottom) {
            painter.fillRect(fX, fY, fWidth, fHeight);
        }

    }

    @Override
    public void move(int width, int height){
        super.move(width,height);
        if (super.getX() == 0 || super.getX() == 500 - super.getWidth()){
            this.topbottom = true;
            this.color = this.temp;
        }
        if (super.getY() == 0 || super.getY() == 478 - super.getHeight()){
            this.topbottom = false;
        }
    }
}
