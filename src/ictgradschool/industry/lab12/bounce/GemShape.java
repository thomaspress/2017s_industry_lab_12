package ictgradschool.industry.lab12.bounce;

import java.awt.*;

/**
 * Class to represent a simple Oval.
 * 
 * @author Ian Warren
 */
public class GemShape extends Shape {
	/**
	 * Default constructor that creates a ictgradschool.industry.lab12.bounce.OvalShape instance whose instance
	 * variables are set to default values.
	 */
	public GemShape() {
		super();
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.RectangleShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed and direction for horizontal axis.
	 * @param deltaY speed and direction for vertical axis.
	 */
	public GemShape(int x, int y, int deltaX, int deltaY) {
		super(x,y,deltaX,deltaY);
	}

	/**
	 * Creates a ictgradschool.industry.lab12.bounce.OvalShape instance with specified values for instance
	 * variables.
	 * @param x x position.
	 * @param y y position.
	 * @param deltaX speed (pixels per move call) and direction for horizontal
	 *        axis.
	 * @param deltaY speed (pixels per move call) and direction for vertical
	 *        axis.
	 * @param width width in pixels.
	 * @param height height in pixels.
	 */
	public GemShape(int x, int y, int deltaX, int deltaY, int width, int height) {
		super(x,y,deltaX,deltaY,width,height);
	}
	
	/**
	 * Paints this ictgradschool.industry.lab12.bounce.RectangleShape object using the supplied ictgradschool.industry.lab12.bounce.Painter object.
	 */
	@Override
	public void paint(Painter painter) {
	    Polygon polygon;
	    int localX = super.fWidth ;
	    int localY = super.fHeight;

        if (super.fWidth >= 40) {
            int[] xPoints = {fX, fX+20, fX+localX - 20, fX+localX, fX+localX - 20, fX+20};
            int[] yPoints = {fY+ localY/2, fY, fY,fY+localY/2,fY+localY,fY+localY};
            polygon = new Polygon(xPoints, yPoints, 6);
        } else {
            int[] xPoints = {fX,fX+localX/2,fX+localX,fX+localX/2};
            int[] yPoints = {fY+localY/2,fY,fY+localY/2,fY+localY};
            polygon = new Polygon(xPoints, yPoints, 4);
        }
		painter.drawPolygon(polygon);
	}
}
