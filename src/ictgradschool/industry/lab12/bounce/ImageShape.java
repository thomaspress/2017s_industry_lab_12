package ictgradschool.industry.lab12.bounce;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.IOException;

public class ImageShape extends Shape {
    ImageShape(){
        super();
    }

    ImageShape(int x, int y, int deltaX, int deltaY) {
        super(x, y, deltaX, deltaY);
    }

    ImageShape(int x, int y, int deltaX, int deltaY, int width, int height) {
        super(x, y, deltaX, deltaY, width, height);
    }



    @Override
    public void paint(Painter painter) {
        BufferedImage img = null;

        try {
            img = ImageIO.read(new File("bouncing.jpg"));
        }catch (IOException e){
            System.out.println(e);
        }
        ImageObserver observer = null;
        painter.drawImage(img,fX,fY,fWidth,fHeight,observer);
    }
}
